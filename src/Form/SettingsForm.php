<?php

namespace Drupal\file_upload_size_limit\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure File Upload Limit settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'file_upload_size_limit.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'file_upload_size_limit.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('file_upload_size_limit.settings');

    $form['multiple_upload_size_limit_type'] = [
      '#title' => $this->t('Total upload limit based on value from'),
      '#description' => $this->t('Configure how the total upload limit size is determined.'),
      '#type' => 'select',
      '#default_value' => $config->get('multiple_upload_size_limit_type'),
      '#options' => [
        'unlimited' => $this->t('Unlimited'),
        'match_single' => $this->t('Match single file upload size.'),
        'custom_value' => $this->t('Custom limit')
      ],
    ];

    $form['multiple_upload_size_custom_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Custom limit size in bytes'),
      '#default_value' => $config->get('multiple_upload_size_custom_limit'),
      '#states' => [
        'visible' => [
          'select[name="multiple_upload_size_limit_type"]' => ['value' => 'custom_value'],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Config\Config $config */
    $config = $this->config('file_upload_size_limit.settings');
    $config->set('multiple_upload_size_limit_type', $form_state->getValue('multiple_upload_size_limit_type'));

    if ($config->get('multiple_upload_size_limit_type') === 'custom_value') {
      $config->set('multiple_upload_size_custom_limit', $form_state->getValue('multiple_upload_size_custom_limit'));
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
