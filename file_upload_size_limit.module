<?php

use Drupal\file\Element\ManagedFile;

/**
 * Implements hook_library_info_alter().
 */
function file_upload_size_limit_library_info_alter(&$libraries, $extension) {
  // Prepend our library so that our validation is triggered before the upload
  // that is also triggered by an onChange event listener.
  if ($extension === 'file') {
    if (isset($libraries['drupal.file'])) {
      $libraries['drupal.file']['dependencies'][] = 'file_upload_size_limit/upload_limit';
    }
  }
}

/**
 * Implements hook_element_info_alter().
 */
function file_upload_size_limit_element_info_alter(array &$info) {
  // Support all managed file types (core and contrib).
  foreach ($info as $type => &$type_info) {
    if (isset($type_info['#process'][0][0]) && is_a($type_info['#process'][0][0], ManagedFile::class, TRUE)) {
      $type_info['#process'][] = 'file_upload_size_limit_managed_file_process';
    }
  }
}

/**
 * Returns the multiple upload limit.
 *
 * @param int $single_limit
 *   The single limit.
 *
 * @return array
 *   A list containing the limit type and the limit size.
 */
function file_upload_size_limit_get_multiple_limit(int $single_limit) {
  $multiple_limit_type = NULL;
  $multiple_limit_size = NULL;

  $config = \Drupal::config('file_upload_size_limit.settings');
  $multiple_limit_type = $config->get('multiple_upload_size_limit_type');
  switch ($multiple_limit_type) {
    case 'unlimited':
      $multiple_limit_size = PHP_INT_MAX;
      break;
    case 'match_single':
      $multiple_limit_size = $single_limit;
      break;
    case 'custom_value':
      $multiple_limit_size = $config->get('multiple_upload_size_custom_limit');
      if (empty($multiple_limit_size)) {
        throw new LogicException("The custom multiple upload limit should be used, but it is not configured.");
      }
      break;
    default:
      throw new LogicException("No valid multiple upload limit type has been configured.");
  }

  return [$multiple_limit_type, $multiple_limit_size];
}

/**
 * Process function to extends the managed_file element type.
 */
function file_upload_size_limit_managed_file_process($element, &$form_state, $form) {
  // Add the field size to the page as JavaScript settings.
  if (isset($element['#upload_validators']['file_validate_size'][0])) {
    $file_size = $element['#upload_validators']['file_validate_size'][0];
    $element['#attached']['drupalSettings']['fileUploadSizeLimit']['single_size'] = $file_size;

    [$multiple_limit_type, $multiple_limit_size] = file_upload_size_limit_get_multiple_limit($file_size);
    $element['#attached']['drupalSettings']['fileUploadSizeLimit']['multiple_limit_type'] = $multiple_limit_type;
    $element['#attached']['drupalSettings']['fileUploadSizeLimit']['multiple_size'] = $multiple_limit_size;
  }

  return $element;
}

/**
 * Implements hook_preprocess_HOOK() for file_upload_help templates.
 */
function file_upload_size_limit_preprocess_file_upload_help(&$variables) {
  $upload_validators = $variables['upload_validators'];
  $cardinality = $variables['cardinality'];

  if (isset($upload_validators['file_validate_size']) && isset($cardinality) && ($cardinality == - 1 || $cardinality > 1)) {
    $file_size = $upload_validators['file_validate_size'][0];
    [$multiple_limit_type, $multiple_limit_size] = file_upload_size_limit_get_multiple_limit($file_size);
    if ($multiple_limit_type !== 'unlimited') {
      $variables['descriptions'][] = t('Total size limit of all files - @size.', ['@size' => format_size($multiple_limit_size)]);
    }
  }
}
