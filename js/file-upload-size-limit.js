(function (Drupal, drupalSettings, once) {

  /**
   * File upload limit functions.
   *
   * @namespace
   */
  Drupal.fileUploadSizeLimit = Drupal.fileUploadSizeLimit || {

    /**
     * Client-side file input validation of file size.
     *
     * @name Drupal.fileUploadSizeLimit.uploadLimitSizeValidate
     *
     * @param {document#event:change} event
     *   The event triggered. For example `change.fileValidate`.
     */
    uploadLimitSizeValidate(event) {
      event.preventDefault();
      // Remove any previous errors.
      const $errorMessages = document.querySelectorAll('.file-upload-js-error')
      for (let i = 0; i < $errorMessages.length; i++) {
        $errorMessages[i].remove();
      }

      // Define variables.
      var singleFileSize = this.single_size;
      var multipleLimitType = this.multiple_limit_type;
      var multipleFileSize = this.multiple_size;

      // Check uploaded files if they are exists and singleFileSize is defined.
      const $fileInput = event.target;

      if ($fileInput.value && $fileInput.files && $fileInput.files.length) {
        // Calculate the size of the uploaded files.
        var totalUploadSize = 0;

        for (var i = 0; i < $fileInput.files.length; i++) {
          var size = $fileInput.files[i].size;
          // Check the single file size limit.
          if (size > singleFileSize) {
            var error = Drupal.t('The size of the uploaded file (%file_size MB) exceeds the maximum allowed size for a single file of %single_file_max_size MB.', {
              '%file_size': parseFloat((size / (1024*1024)).toFixed(2)),
              '%single_file_max_size': parseFloat((singleFileSize / (1024*1024)).toFixed(2)),
            });
            Drupal.fileUploadSizeLimit.showError(event, $fileInput, error);
          }

          // Sum the total file upload size.
          totalUploadSize += size;
        }

        // Check the total multiple file size limit.
        if (multipleLimitType !== 'unlimited' && (totalUploadSize > multipleFileSize)) {
          var error = Drupal.t('The total size of the uploaded file(s) (%totalUploadSize MB) exceeds the maximum allowed size of %allowed_total_size MB.', {
            '%totalUploadSize': parseFloat((totalUploadSize / (1024*1024)).toFixed(2)),
            '%allowed_total_size': parseFloat((multipleFileSize / (1024*1024)).toFixed(2)),
          });
          Drupal.fileUploadSizeLimit.showError(event, $fileInput, error);
        }
      }
    },

    showError(event, $fileInput, error) {
      const $fileItem = $fileInput.closest('.js-form-managed-file');

      const message = document.createElement('div');
      message.classList.add('messages', 'messages--error', 'file-upload-js-error');
      message.ariaLive = 'polite';
      message.innerHTML = error;
      $fileItem.prepend(message);

      $fileInput.value = '';

      // Cancel all other change event handlers.
      event.stopImmediatePropagation();
    }
  };

  /**
   * Attach behaviors to managed file element upload fields.
   */
  Drupal.behaviors.fileUploadSizeLimitAutoAttach = {
    attach(context, settings) {
      let elements;

      const initFileValidation = (selector) => {
        context.querySelectorAll(selector)

        once('fileUploadSizeLimitValidate', selector, context).forEach((element) => {
          element.addEventListener('change', Drupal.fileUploadSizeLimit.uploadLimitSizeValidate.bind(settings.fileUploadSizeLimit))
        })
      }

      if (settings.file && settings.file.elements) {
        elements = settings.file.elements;
        Object.keys(elements).forEach(initFileValidation);
      }
    },
    detach(context, settings, trigger) {
      const removeFileValidation = (selector) => {
        const $removedElements = once.remove('fileUploadSizeLimitValidate', selector, context)

        $removedElements.forEach(($removedElement) => {
          $removedElement.removeEventListener('change', Drupal.fileUploadSizeLimit.uploadLimitSizeValidate);
        });
      }

      if (trigger === 'unload' && settings.file && settings.file.elements) {
        elements = settings.file.elements;
        Object.keys(elements).forEach(removeFileValidation);
      }
    },
  };

})(Drupal, drupalSettings, once);
